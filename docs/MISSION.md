Astian Spika is a messenger for the next decade. Based on already established
internet standards that have been around for over ten years Spika isn’t
trying to replace current commercial messengers. It will simply outlive them.
Commercial, closed source products are coming and going. 15 years ago we had ICQ
which was replaced by Skype. MySpace was replaced by Facebook. WhatsApp and
Hangouts will disappear soon. Internet standards however stick around. People
are still using IRC and e-mail even though these protocols have been around for
decades. Utilizing proven standards doesn’t mean one can not evolve. GMail has
revolutionized the way we look at e-mail. Firefox and Chrome have changed the
way we use the Web. Spika will change the way we look at instant
messaging. Being less obtrusive than a telephone call instant messaging has
always played an important role in modern society. Spika will show that
instant messaging can be fast, reliable and private. Spika will not
force its security and privacy aspects upon the user. For those willing to use
encryption Spika will make it as uncomplicated as possible. However
Spika is aware that end-to-end encryption by the very principle isn’t
trivial. Instead of trying the impossible and making encryption easier than
comparing a fingerprint Spika will try to educate the willing user and
explain the necessary steps and the reasons behind them. Those unwilling to
learn about encryption will still be protected by the design principals of
Spika. Spika will simply not share or generate certain
information for example by encouraging the use of federated servers.
Spika will always utilize the best available standards for encryption
and media encoding instead of reinventing the wheel. However it isn’t afraid to
break with behavior patterns that have been proven ineffective.

Spika is applicable both in business environments, together with Astian Cloud, you can also use it in your day to day as an alternative to WhatsApp, Signal, Telegram is extremely fast with point-to-point encryption.
