package org.astianspika.android.crypto.axolotl;

public interface OnMessageCreatedCallback {
    void run(XmppAxolotlMessage message);
}
