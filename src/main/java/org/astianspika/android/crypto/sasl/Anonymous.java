package org.astianspika.android.crypto.sasl;

import java.security.SecureRandom;

import org.astianspika.android.entities.Account;
import org.astianspika.android.xml.TagWriter;

public class Anonymous extends SaslMechanism {

    public Anonymous(TagWriter tagWriter, Account account, SecureRandom rng) {
        super(tagWriter, account, rng);
    }

    @Override
    public int getPriority() {
        return 0;
    }

    @Override
    public String getMechanism() {
        return "ANONYMOUS";
    }

    @Override
    public String getClientFirstMessage() {
        return "";
    }
}
