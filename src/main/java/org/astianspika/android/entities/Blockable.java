package org.astianspika.android.entities;

import org.astianspika.android.xmpp.Jid;

public interface Blockable {
    boolean isBlocked();

    boolean isDomainBlocked();

    Jid getBlockedJid();

    Jid getJid();

    Account getAccount();
}
