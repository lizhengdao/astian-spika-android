package org.astianspika.android.ui;

public interface UiInformableCallback<T> extends UiCallback<T> {
    void inform(String text);
}