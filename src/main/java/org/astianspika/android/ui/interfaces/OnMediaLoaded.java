package org.astianspika.android.ui.interfaces;

import java.util.List;

import org.astianspika.android.ui.util.Attachment;

public interface OnMediaLoaded {

    void onMediaLoaded(List<Attachment> attachments);
}