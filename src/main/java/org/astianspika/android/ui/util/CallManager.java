package org.astianspika.android.ui.util;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.widget.Toast;

import com.google.common.base.Optional;

import java.util.ArrayList;
import java.util.List;

import org.astianspika.android.Config;
import org.astianspika.android.R;
import org.astianspika.android.entities.Account;
import org.astianspika.android.entities.Contact;
import org.astianspika.android.entities.Conversation;
import org.astianspika.android.ui.RtpSessionActivity;
import org.astianspika.android.ui.XmppActivity;
import org.astianspika.android.utils.Namespace;
import org.astianspika.android.xmpp.Jid;
import org.astianspika.android.xmpp.jingle.AbstractJingleConnection;
import org.astianspika.android.xmpp.jingle.JingleConnectionManager;
import org.astianspika.android.xmpp.jingle.Media;
import org.astianspika.android.xmpp.jingle.OngoingRtpSession;
import org.astianspika.android.xmpp.jingle.RtpCapability;

import static org.astianspika.android.ui.ConversationFragment.REQUEST_START_AUDIO_CALL;
import static org.astianspika.android.ui.ConversationFragment.REQUEST_START_VIDEO_CALL;

public class CallManager {

    public static void checkPermissionAndTriggerAudioCall(XmppActivity activity, Conversation conversation) {
        if (activity.mUseTor || conversation.getAccount().isOnion()) {
            Toast.makeText(activity, R.string.disable_tor_to_make_call, Toast.LENGTH_SHORT).show();
            return;
        }
        if (hasPermissions(REQUEST_START_AUDIO_CALL, activity, Manifest.permission.RECORD_AUDIO)) {
            triggerRtpSession(RtpSessionActivity.ACTION_MAKE_VOICE_CALL, activity, conversation);
        }
    }

    public static void checkPermissionAndTriggerVideoCall(XmppActivity activity, Conversation conversation) {
        if (activity.mUseTor || conversation.getAccount().isOnion()) {
            Toast.makeText(activity, R.string.disable_tor_to_make_call, Toast.LENGTH_SHORT).show();
            return;
        }
        if (hasPermissions(REQUEST_START_VIDEO_CALL, activity, Manifest.permission.CAMERA)) {
            triggerRtpSession(RtpSessionActivity.ACTION_MAKE_VIDEO_CALL, activity, conversation);
        }
    }

    public static void triggerRtpSession(final String action, XmppActivity activity, Conversation conversation) {
        if (activity.xmppConnectionService.getJingleConnectionManager().isBusy()) {
            Toast.makeText(activity, R.string.only_one_call_at_a_time, Toast.LENGTH_LONG).show();
            return;
        }

        final Contact contact = conversation.getContact();
        if (contact.getPresences().anySupport(Namespace.JINGLE_MESSAGE)) {
            triggerRtpSession(contact.getAccount(), contact.getJid().asBareJid(), action, activity);
        } else {
            final RtpCapability.Capability capability;
            if (action.equals(RtpSessionActivity.ACTION_MAKE_VIDEO_CALL)) {
                capability = RtpCapability.Capability.VIDEO;
            } else {
                capability = RtpCapability.Capability.AUDIO;
            }
            PresenceSelector.selectFullJidForDirectRtpConnection(activity, contact, capability, fullJid -> {
                triggerRtpSession(contact.getAccount(), fullJid, action, activity);
            });
        }
    }

    private static void triggerRtpSession(final Account account, final Jid with, final String action, XmppActivity activity) {
        final Intent intent = new Intent(activity, RtpSessionActivity.class);
        intent.setAction(action);
        intent.putExtra(RtpSessionActivity.EXTRA_ACCOUNT, account.getJid().toEscapedString());
        intent.putExtra(RtpSessionActivity.EXTRA_WITH, with.toEscapedString());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    public static void returnToOngoingCall(XmppActivity activity, Conversation conversation) {
        final Optional<OngoingRtpSession> ongoingRtpSession = activity.xmppConnectionService.getJingleConnectionManager().getOngoingRtpConnection(conversation.getContact());
        if (ongoingRtpSession.isPresent()) {
            final OngoingRtpSession id = ongoingRtpSession.get();
            final Intent intent = new Intent(activity, RtpSessionActivity.class);
            intent.putExtra(RtpSessionActivity.EXTRA_ACCOUNT, id.getAccount().getJid().asBareJid().toEscapedString());
            intent.putExtra(RtpSessionActivity.EXTRA_WITH, id.getWith().toEscapedString());
            if (id instanceof AbstractJingleConnection.Id) {
                intent.setAction(Intent.ACTION_VIEW);
                intent.putExtra(RtpSessionActivity.EXTRA_SESSION_ID, id.getSessionId());
            } else if (id instanceof JingleConnectionManager.RtpSessionProposal) {
                if (((JingleConnectionManager.RtpSessionProposal) id).media.contains(Media.VIDEO)) {
                    intent.setAction(RtpSessionActivity.ACTION_MAKE_VIDEO_CALL);
                } else {
                    intent.setAction(RtpSessionActivity.ACTION_MAKE_VOICE_CALL);
                }
            }
            activity.startActivity(intent);
        }
    }

    private static boolean hasPermissions(int requestCode, XmppActivity activity, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final List<String> missingPermissions = new ArrayList<>();
            for (String permission : permissions) {
                if (Config.ONLY_INTERNAL_STORAGE && permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    continue;
                }
                if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    missingPermissions.add(permission);
                }
            }
            if (missingPermissions.size() == 0) {
                return true;
            } else {
                activity.requestPermissions(missingPermissions.toArray(new String[missingPermissions.size()]), requestCode);
                return false;
            }
        } else {
            return true;
        }
    }
}
