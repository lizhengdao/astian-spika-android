package org.astianspika.android.xmpp;

import org.astianspika.android.entities.Account;

public interface OnAdvancedStreamFeaturesLoaded {
    public void onAdvancedStreamFeaturesAvailable(final Account account);
}
