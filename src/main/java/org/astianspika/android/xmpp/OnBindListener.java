package org.astianspika.android.xmpp;

import org.astianspika.android.entities.Account;

public interface OnBindListener {
    public void onBind(Account account);
}
