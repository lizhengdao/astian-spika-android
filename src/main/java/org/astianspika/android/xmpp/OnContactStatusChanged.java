package org.astianspika.android.xmpp;

import org.astianspika.android.entities.Contact;

public interface OnContactStatusChanged {
    public void onContactStatusChanged(final Contact contact, final boolean online);
}
