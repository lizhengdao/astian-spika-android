package org.astianspika.android.xmpp;

import org.astianspika.android.entities.Account;
import org.astianspika.android.xmpp.stanzas.IqPacket;

public interface OnIqPacketReceived extends PacketReceived {
    void onIqPacketReceived(Account account, IqPacket packet);
}
