package org.astianspika.android.xmpp;

import org.astianspika.android.crypto.axolotl.AxolotlService;

public interface OnKeyStatusUpdated {
    public void onKeyStatusUpdated(AxolotlService.FetchStatus report);
}
