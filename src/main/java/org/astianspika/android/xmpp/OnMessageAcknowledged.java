package org.astianspika.android.xmpp;

import org.astianspika.android.entities.Account;

public interface OnMessageAcknowledged {
    boolean onMessageAcknowledged(Account account, String id);
}
