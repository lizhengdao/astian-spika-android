package org.astianspika.android.xmpp;

import org.astianspika.android.entities.Account;
import org.astianspika.android.xmpp.stanzas.MessagePacket;

public interface OnMessagePacketReceived extends PacketReceived {
    public void onMessagePacketReceived(Account account, MessagePacket packet);
}
