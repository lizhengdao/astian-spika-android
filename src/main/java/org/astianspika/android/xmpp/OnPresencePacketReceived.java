package org.astianspika.android.xmpp;

import org.astianspika.android.entities.Account;
import org.astianspika.android.xmpp.stanzas.PresencePacket;

public interface OnPresencePacketReceived extends PacketReceived {
    public void onPresencePacketReceived(Account account, PresencePacket packet);
}
