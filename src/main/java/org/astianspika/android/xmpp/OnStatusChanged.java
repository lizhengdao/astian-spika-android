package org.astianspika.android.xmpp;

import org.astianspika.android.entities.Account;

public interface OnStatusChanged {
    public void onStatusChanged(Account account);
}
