package org.astianspika.android.xmpp.jingle;

import org.astianspika.android.entities.DownloadableFile;

public interface OnFileTransmissionStatusChanged {
    void onFileTransmitted(DownloadableFile file);

    void onFileTransferAborted();
}
