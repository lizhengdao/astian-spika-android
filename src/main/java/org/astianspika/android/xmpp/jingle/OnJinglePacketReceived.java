package org.astianspika.android.xmpp.jingle;

import org.astianspika.android.entities.Account;
import org.astianspika.android.xmpp.PacketReceived;
import org.astianspika.android.xmpp.jingle.stanzas.JinglePacket;

public interface OnJinglePacketReceived extends PacketReceived {
    void onJinglePacketReceived(Account account, JinglePacket packet);
}
