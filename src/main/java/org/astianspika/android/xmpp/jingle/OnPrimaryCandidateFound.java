package org.astianspika.android.xmpp.jingle;

public interface OnPrimaryCandidateFound {
    void onPrimaryCandidateFound(boolean success, JingleCandidate canditate);
}
