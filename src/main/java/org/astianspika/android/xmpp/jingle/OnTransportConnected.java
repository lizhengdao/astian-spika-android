package org.astianspika.android.xmpp.jingle;

public interface OnTransportConnected {
    public void failed();

    public void established();
}
