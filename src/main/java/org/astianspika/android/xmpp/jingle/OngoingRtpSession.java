package org.astianspika.android.xmpp.jingle;

import org.astianspika.android.entities.Account;
import org.astianspika.android.xmpp.Jid;

public interface OngoingRtpSession {
    Account getAccount();
    Jid getWith();
    String getSessionId();
}
