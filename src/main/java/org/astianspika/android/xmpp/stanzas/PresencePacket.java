package org.astianspika.android.xmpp.stanzas;

public class PresencePacket extends AbstractAcknowledgeableStanza {

    public PresencePacket() {
        super("presence");
    }
}
