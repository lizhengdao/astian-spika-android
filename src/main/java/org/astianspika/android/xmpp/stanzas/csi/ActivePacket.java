package org.astianspika.android.xmpp.stanzas.csi;

import org.astianspika.android.xmpp.stanzas.AbstractStanza;

public class ActivePacket extends AbstractStanza {
    public ActivePacket() {
        super("active");
        setAttribute("xmlns", "urn:xmpp:csi:0");
    }
}
