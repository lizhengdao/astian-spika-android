package org.astianspika.android.xmpp.stanzas.streammgmt;

import org.astianspika.android.xmpp.stanzas.AbstractStanza;

public class EnablePacket extends AbstractStanza {

    public EnablePacket(int smVersion) {
        super("enable");
        this.setAttribute("xmlns", "urn:xmpp:sm:" + smVersion);
        this.setAttribute("resume", "true");
    }

}
