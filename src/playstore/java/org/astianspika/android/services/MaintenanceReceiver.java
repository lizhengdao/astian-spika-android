package org.astianspika.android.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;

import org.astianspika.android.BuildConfig;
import org.astianspika.android.Config;
import org.astianspika.android.utils.Compatibility;

public class MaintenanceReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(Config.LOGTAG, "received intent in maintenance receiver");
        final String string = BuildConfig.APPLICATION_ID + ".RENEW_INSTANCE_ID";
        if (string.equals(intent.getAction())) {
        }
    }
}